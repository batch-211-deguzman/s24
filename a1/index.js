console.log("Hello World!");

//Exponent Operator

const getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

// Template Literal

let address = ["I", "live", "at", "258", "Washington", "Ave", "NW,", "California", "90011"]

let sentence1 = `${address[0]} ${address[1]} ${address[2]} ${address[3]} ${address[4]} ${address[5]} ${address[6]} ${address[7]} ${address[8]} ${address[9]}`;
console.log(sentence1);

// Object Destructing

const animal = {
	Name1: "Lolong was a saltwater crocodile.",
	Name2: "He weighted at 1075 kgs with a mesaurement of 20 ft 3 in."
};

console.log(`${animal.Name1} ${animal.Name2}`);

// Arrow Functions

const array = [1,2,3,4,5];

array.forEach(function(array){
	console.log(array);
});

let reducedNumber = array.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue;
});

console.log(reducedNumber);

	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	const myDog = new Dog();

	myDog.name = "The King";
	myDog.age = 1;
	myDog.breed = "Golden Retriever";
	console.log(myDog);